#include "MainFrame.h"
#include "wx/gtk/colour.h"
#include "wx/wx/gdicmn.h"
#include "../graphics/graphicshelper/graphhelper.h"
#include <wx/listbook.h>
#include <wx/wx.h>

MainFrame::MainFrame(const wxString& title) : wxFrame(nullptr, wxID_ANY, title) {
	panel = new wxPanel(this);
	panel->SetBackgroundColour(*wxLIGHT_GREY);

	//First Row (Check Find functions)
	findStatText = new wxStaticText(panel, wxID_ANY, "Find functions:", wxPoint(30, 20), wxDefaultSize);
	wxFont font = findStatText->GetFont();
	font.SetPointSize(12);
	font.SetWeight(wxFONTWEIGHT_BOLD);
	findStatText->SetFont(font);

	txtSlider = new wxStaticText(panel, wxID_ANY, "Number of elements: ", wxPoint(30, 45), wxDefaultSize);
	slider = new wxSlider(panel, wxID_ANY, 10, 10, 10000, wxPoint(30, 65), wxSize(200, -1), wxSL_LABELS);

	btnCreateArray = new wxButton(panel, wxID_ANY, "Create Array", wxPoint(30, 110), wxSize(120, -1));
	btnPrintArray = new wxButton(panel, wxID_ANY, "Print Array", wxPoint(30, 140), wxSize(120, -1));

	findFuns.Add("Bin Search");
	findFuns.Add("Hash Table");
	findFuns.Add("Bin Tree");
	findFuns.Add("BTree");
	choiceFindFun = new wxChoice(panel, wxID_ANY, wxPoint(30, 180), wxDefaultSize, findFuns);

	btnFind = new wxButton(panel, wxID_ANY, "Find", wxPoint(140, 180), wxSize(60, -1));
	btnFind->SetBackgroundColour(wxColor(70, 205, 70));

	txtValToFind = new wxStaticText(panel, wxID_ANY, "Enter Value \nto find:", wxPoint(160, 107), wxDefaultSize);
	txtCtrlValToFind = new wxTextCtrl(panel, wxID_ANY, "", wxPoint(160, 140), wxSize(80, -1));

	staticLine1 = new wxStaticLine(panel, wxID_ANY, wxPoint(250, 10), wxSize(5, 580));
	staticLine2 = new wxStaticLine(panel, wxID_ANY, wxPoint(10, 215), wxSize(235, 5));

	//Second Row (Find Most Popular Subexpression)
	mostPopStatText = new wxStaticText(panel, wxID_ANY, "Most Popular Subexpression:", wxPoint(30, 225), wxDefaultSize);
	mostPopStatText->SetFont(font);

	txtEnterExp = new wxStaticText(panel, wxID_ANY, "Enter expression", wxPoint(30, 275), wxDefaultSize);

	txtCtrlExp = new wxTextCtrl(panel, wxID_ANY, "", wxPoint(30, 295), wxSize(200, -1));

	btnFindExp = new wxButton(panel, wxID_ANY, "Find", wxPoint(173, 260), wxSize(60, -1));
	btnFindExp->SetBackgroundColour(wxColor(70, 205, 70));

	staticLine3 = new wxStaticLine(panel, wxID_ANY, wxPoint(10, 330), wxSize(235, 5));

	//Third Row (Matrix Processing)
	matStatText = new wxStaticText(panel, wxID_ANY, "Matrix Processing:", wxPoint(30, 340), wxDefaultSize);
	matStatText->SetFont(font);

	txtSliderMat = new wxStaticText(panel, wxID_ANY, "Number of rows and colums: ", wxPoint(30, 365), wxDefaultSize);
	sliderMat = new wxSlider(panel, wxID_ANY, 2, 1, 5, wxPoint(30, 385), wxSize(200, -1), wxSL_LABELS);

	btnCreateMatrix = new wxButton(panel, wxID_ANY, "Create Matrix", wxPoint(30, 430), wxSize(120, -1));
	btnPrintMatrix = new wxButton(panel, wxID_ANY, "Print Matrix", wxPoint(30, 460), wxSize(120, -1));

	btnProcessMat = new wxButton(panel, wxID_ANY, "Process", wxPoint(170, 440), wxSize(70, -1));
	btnProcessMat->SetBackgroundColour(wxColor(70, 205, 70));

	staticLine4 = new wxStaticLine(panel, wxID_ANY, wxPoint(10, 495), wxSize(235, 5));

	//Fourf Rov (Hystogram)

	HystogramStatText = new wxStaticText(panel, wxID_ANY, "Histogram", wxPoint(30, 505), wxSize(5,5));
	HystogramStatText->SetFont(font);

	btnCreateHG = new wxButton(panel, wxID_ANY, "Create Histogram", wxPoint(30, 525), wxSize(120, 30));
	sliderHG = new wxSlider(panel, wxID_ANY, 1, 1, 100, wxPoint(30, 555), wxSize(200, -1), wxSL_LABELS);

	txtPrint = new wxTextCtrl(panel, wxID_ANY, "", wxPoint(270, 20), wxSize(520, 545), wxTE_MULTILINE);

	btnClearTxtPrint = new wxButton(panel, wxID_ANY, "Clear", wxPoint(710, 570), wxSize(80, -1));
	//Bindings
	btnClearTxtPrint->Bind(wxEVT_BUTTON, &MainFrame::OnClearTextPrintBtnClicked, this);
	btnCreateArray->Bind(wxEVT_BUTTON, &MainFrame::OnCreateArrayBtnClicked, this);
	btnPrintArray->Bind(wxEVT_BUTTON, &MainFrame::OnPrintArrayBtnClicked, this);
	btnFind->Bind(wxEVT_BUTTON, &MainFrame::OnFindBtnClicked, this);
	btnFindExp->Bind(wxEVT_BUTTON, &MainFrame::OnFindMostPopularBtnClicked, this);
	btnCreateMatrix->Bind(wxEVT_BUTTON, &MainFrame::OnCreateMatrixBtnClicked, this);
	btnPrintMatrix->Bind(wxEVT_BUTTON, &MainFrame::OnPrintMatrixBtnClicked, this);
	btnProcessMat->Bind(wxEVT_BUTTON, &MainFrame::OnProcessMatrixBtnClicked, this);
	btnCreateHG->Bind(wxEVT_BUTTON, &MainFrame::OnHGBtnClicked, this);
}

void MainFrame::OnCreateArrayBtnClicked(wxCommandEvent& evt) {
	presenter.createArr(slider->GetValue());

	txtPrint->WriteText("Array was created!\n\n");
}

void MainFrame::OnPrintArrayBtnClicked(wxCommandEvent& evt) {
	string arr = presenter.getArr();

	txtPrint->WriteText("Array: " + arr + "\n\n");
}

void MainFrame::OnFindBtnClicked(wxCommandEvent& evt) {
	string findType = findFuns[choiceFindFun->GetSelection()].ToStdString();
	string valueToFind = txtCtrlValToFind->GetValue().ToStdString();

	pair<string, bool> resOfFind = presenter.find(findType, valueToFind);

	bool res = resOfFind.second;

	if(res)
		txtPrint->WriteText("Value: " + valueToFind + " Exists in Array" + "\n");
	else
		txtPrint->WriteText("Value: " + valueToFind + " Doesn't exist in Array" + "\n");

	txtPrint->WriteText("Finding time: " + resOfFind.first + "\n");
}

void MainFrame::OnClearTextPrintBtnClicked(wxCommandEvent& evt) {
	txtPrint->Clear();
}

void MainFrame::OnFindMostPopularBtnClicked(wxCommandEvent& evt) {
	string res = presenter.findMostPopularExp(txtCtrlExp->GetValue().ToStdString());

	txtPrint->WriteText("Most Popular Subexpression is: " + res + "\n\n");
}

void MainFrame::OnCreateMatrixBtnClicked(wxCommandEvent& evt) {
	presenter.createMat(sliderMat->GetValue());

	txtPrint->WriteText("Matrix was created!\n\n");
}

void MainFrame::OnPrintMatrixBtnClicked(wxCommandEvent& evt) {
	string matrix = presenter.getMat();

	txtPrint->WriteText("Matrix: \n" + matrix + "\n\n");
}

void MainFrame::OnProcessMatrixBtnClicked(wxCommandEvent& evt) {
	string res = presenter.processMat();

	txtPrint->WriteText("Result: \n" + res + "\n\n");
}

void MainFrame::OnHGBtnClicked(wxCommandEvent& evt) {
	string HGres = presenter.getHystogram(sliderHG->GetValue());
	txtPrint->WriteText("Histogram: \n" + HGres + "\n\n");
}


