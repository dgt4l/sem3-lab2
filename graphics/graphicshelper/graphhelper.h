#pragma once
#include "vector"

#include "../../core/DynamicArray/ArraySequence.hpp"
#include "../../core/LinkedList/LinkedListSequence.hpp"
#include "../../core/binsearch.hpp"
#include "../../core/hashtable.hpp"
#include "../../core/binarytree.hpp"
#include "../../core/btree.hpp"
#include "../../core/hystogram.hpp"
#include "../../core/sort.hpp"


class Presenter {
private:
	int size;
	int matrix_size;
	ArraySequence<int>* arr;
	int** matrix;
public:
	void createArr(int size);
	string getArr();
	pair<string, bool> find(string findType, string value); 
	string findMostPopularExp(string s);
	void createMat(int size);
	string getMat();
	string processMat();
	string getHystogram(int range);
	tuple<vector<double>, vector<double>, vector<double>> getPointsForChart();
};