#pragma once

#include <map>
#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include "sort.hpp"
#include "DynamicArray/ArraySequence.hpp"

template<class U>
class Hystogram{
public:
    static vector<std::pair<int, std::string>> create_hystogram(U* arr, int range);
};

template<class U>
vector<std::pair<int, std::string>> Hystogram<U>::create_hystogram(U* arr, int range){
    ArraySequence<int>* temparr = arr;
    string key;
    vector<std::pair<int, std::string>> v;
    int max_el = arr->findMax();
    int rows = (max_el / range) + (max_el % range != 0);
    for (int i = 1; i <= rows; i++){
        if(range * i > max_el){
            key = "[" + (std::to_string((range * (i-1)) + 1)) + "-" + (std::to_string(max_el) + "]");
        } else {
            key = "[" + (std::to_string((range * (i-1)) + 1)) + "-" + (std::to_string(range * i) + "]");
        }
        v.push_back({0, "values in " + key + ": "});
    }
    for(int i = 0; i < temparr->getLength(); i++) {
        for(int j = 1; j <= rows; j++) {
            if(temparr->get(i) <= (range * j) && temparr->get(i) > (range * (j-1))) {
                v[j-1].first +=  1;
                v[j-1].second += (std::to_string(temparr->get(i)) + " ");
            }
        }
    }

    return v;
}
