#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>
#include "DynamicArray/ArraySequence.hpp"
#include "LinkedList/LinkedList.hpp"
#include "LinkedList/LinkedListSequence.hpp"
#include "sort.hpp"
#include "binarytree.hpp"
#include "hashtable.hpp"
#include "binsearch.hpp"
#include "btree.cpp"
#include "btree.hpp"
#include "hystogram.hpp"

using namespace std;


bool BinSearchTest(ArraySequence<int> &arr1){
    bool ans1 = BinSearh<int, ArraySequence<int>>::bin_Search(&arr1, 10);
    bool ans2 = BinSearh<int, ArraySequence<int>>::bin_Search(&arr1, 100);
    if(ans1 && ans2){
        return true;
    } else {
        return false;
    }
}

bool BinTreeTest(BinaryTree<int> tree){
    bool ans1 = tree.find(10);
    bool ans2 = tree.find(20);
    bool ans3 = tree.find(30);
    bool ans4 = tree.find(40);
    bool ans5 = tree.find(50);
    bool ans6 = tree.find(60);
    bool ans7 = tree.find(70);
    bool ans8 = tree.find(80);
    bool ans9 = tree.find(90);
    bool ans10 = tree.find(100);
    if(ans1 && ans2 && ans3 && ans4 && ans5 && ans6 && ans7 && ans8 && ans9 && ans10){
        return true;
    } else{
        return false;
    }
}

bool BTreeTest(bTree btree){
    bool ans1 = BTreeSearch(btree, 10);
    bool ans2 = BTreeSearch(btree, 100);
    if(ans1 && ans2){
        return true;
    } else{
        return false;
    }
}

void Tests(){
    int test_arr1[10] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
    int test_arr2[10] = {16, 22, 36, 50, 78, 84, 87, 87, 91, 95};
    ArraySequence<int> arr1(test_arr1, 10);
    ArraySequence<int> arr2(test_arr2, 10);
    BinaryTree<int> tree1(test_arr1, 10);
    HashTable<int> hashtable1(test_arr1, 10);
    bTree btree1;
    btree1 = BTreeCreate();
    assert(btree1);
    cout << "Starting Tests\n\n";
    BinSearchTest(arr1);
    for(int i = 0; i < arr1.getLength(); i++){
        BTreeInsert(btree1, arr1.get(i));
    }
    if (BinSearchTest(arr1)){
        cout << "BinSearchTest passed\n";
    } else{
        cout << "BinSearchTest failed\n";
    }
    if (BinTreeTest(tree1)){
        cout << "BinTreeTest passed\n";
    } else{
        cout << "BinTreeTest failed\n";
    }
    if (hashtable1.find("10") && hashtable1.find("90")){
        cout << "HashTableTest passed\n";
    } else{
        cout << "HashTableTest failed\n";
    }
    if (BTreeTest(btree1)){
        cout << "BTreeTest passed\n";
    } else{
        cout << "BTreeTest failed\n";
    }
    BTreeDestroy(btree1);
}

int main(){
    Tests();
    return 0;
}
