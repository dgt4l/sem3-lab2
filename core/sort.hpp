#pragma once

#include "DynamicArray/ArraySequence.hpp"
#include "LinkedList/LinkedListSequence.hpp"

template <class T, class U>
class iSorter{
private:
    static void quicksort(U *data, int low, int high) {
        int i = low;
        int j = high;
        T pivot = data->get((i + j) / 2);
        int temp;
        while (i <= j) {
            while (data->get(i) < pivot)
                i++;
            while (data->get(j) > pivot)
                j--;
            if (i <= j) {
                data->swap(i,j);
                i++;
                j--;
            }
        }
        if (j > low)
            quicksort(data, low, j);
        if (i < high)
            quicksort(data, i, high);
    }
public:
    static void quickSort(U* data){
        quicksort(data, 0, data->getLength() - 1);
    }

    static void shellSort(U* data) {
        int n = data->getLength();
        int d = n;
        d = d / 2;
        while (d > 0) {
            for (int i = 0; i < n - d; i++) {
                int j = i;
                while (j >= 0 && data->get(j) > data->get(j + d)){
                    data->swap(j, j + d);
                    j--;
                }
            }
            d = d / 2;
        }
    }

    static void selectionSort(U* data){
        int i, j, min_idx;
        int n = data->getLength();
        for (i = 0; i < n - 1; i++){
            min_idx = i;
            for (j = i + 1; j < n; j++){
                if (data->get(j) < data->get(min_idx))
                    min_idx = j;
            }
            data->swap(min_idx, i);
        }
    }
};