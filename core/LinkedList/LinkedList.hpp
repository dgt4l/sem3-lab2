#pragma once
#include <cstddef>
#include <iostream>
#include <stdexcept>

using namespace std;

template<class T>
class LinkedList{
    struct elem{
        T value;
        elem* next;
    };
private:
    elem* list_head = nullptr;

    elem* getAt(int index) {
        int count = 0;
        if (list_head == NULL)
            return list_head;
        elem* p = list_head;
        while (index != count){
            count++;
            p = p->next;
        }
        return p;
    }

    int list_size = 0;
public:
    LinkedList(){
        list_head = nullptr;
        list_size = 0;
    }

    LinkedList(T* items, int count) : LinkedList(){
        for(int i = 0; i < count; i++){
            append(items[i]);
        }
        list_size = count;
    }

    //  LinkedList (LinkedList<T> &&list){
    //     list_head = list.list_head;
    //     list_size = list.list_size;
    //     list.list_head = nullptr;
    // }

    LinkedList (LinkedList<T> const &list){
        elem* tmp1 = nullptr;
        elem* tmp2 = nullptr;
        if(list.list_head == NULL){
            list_head = NULL;
            return;
        } else{
          list_head = new elem;
          list_head->value = list.list_head->value;
          tmp1 = list_head;
          tmp2 = list.list_head->next;  
        }
        while(tmp2){
            tmp1->next = new elem;
            tmp1 = tmp1->next;
            tmp1->value = tmp2->value;
            tmp2 = tmp2->next;
        }
        tmp1->next = NULL;
        list_size = list.list_size;
    }

   T &get(int index) {
        if (index < 0 || getLength() <= index){
            throw runtime_error("IndexOutOfRange");
        }
        if(list_head == NULL){
            return list_head->value;
        }
        elem* tmp = list_head;
        for (int i = 0; i < index; i++){
            tmp = tmp->next;
        }
        return tmp->value;
    }

    T &operator[](int index){
        return get(index);
    }

    T getFirst(){
        if(list_head == NULL){
            throw runtime_error("IndexOutOfRange");
        } 
        elem* tmp = list_head;
        return tmp->value;
    }

    T getLast(){
        if(list_head == NULL){
            throw runtime_error("IndexOutOfRange");
        }
        elem* tmp = list_head;
        while(tmp->next != NULL){
            tmp = tmp->next;
        }
        return tmp->value;
    }

    int getLength() {
        return list_size;
    }

    void append(T item){
        if(list_head == nullptr){
            prepend(item);
        } else{
            elem* tmp = list_head;
            list_size = list_size + 1;
            while(tmp->next != NULL){
                tmp = tmp->next;
            }
            tmp->next = new elem();
            tmp->next->value = item;
        }
    }

    LinkedList<T> operator+(LinkedList<T>  const &list1){
        LinkedList<T> res = concat(list1);
        return res;
    }

    void prepend(T item){
        elem* tmp = new elem();
        tmp->value = item;
        tmp->next = list_head;
        list_head = tmp;
        list_size = list_size + 1;
    }

    void insertAt(T item, int index){
        if(index > getLength() || index < 0){
            throw runtime_error("IndexOutOfRange");
        }
        if(index == 0){
            prepend(item);
            return;
        }
        elem* tmp = new elem();
        tmp->value = item;
        tmp->next = getAt(index - 1)->next;
        getAt(index - 1)->next = tmp;
    }
    
    LinkedList<T> getSubList(int startindex, int endindex){
        if (startindex < 0 || endindex >= getLength() || endindex < startindex) {
            throw runtime_error("IndexOutOfRange");
        }
        LinkedList<T> res;
        for(int i = 0; i <= endindex; i++){
            if (i >= startindex){
                res.append(get(i));
            }
        }
        return res;
    }

    LinkedList<T> concat(LinkedList<T> const &list) {
        LinkedList<T> resList;
        elem* tmp = list_head;
        do {
            resList.append(tmp->value);
            tmp = tmp->next;
        } while (tmp != NULL);
        elem* tmp1 = list.list_head;
        do {
            resList.append(tmp1->value);
            tmp1 = tmp1->next;
        } while (tmp1 != NULL);
        return resList;
    }

    void print(){
        elem* tmp = list_head;
        while(tmp != NULL){
            cout << tmp->value << " ";
            tmp = tmp->next;
        }
        cout << endl;
    }

    void del_last(){
        if (list_size == 1){
            list_head = nullptr;
            list_size = 0;
            return;
        }
        elem* tmp = list_head;
        elem* tmp1 = list_head;
        while(tmp->next != NULL){
            tmp1 = tmp;
            tmp = tmp ->next;
        }
        delete tmp;
        tmp1->next = nullptr;
        list_size--;
    }

    void swap(int index1, int index2){
        elem* tmp = list_head;
        int count=0;
        int min, max;
        if(index1<index2){
            min = index1;
            max = index2;
        }
        else{
            min = index2;
            max = index1;
        }
        int diff = max - min;
        while(min!=count){
            tmp=  tmp->next;
            count++;
        }
        int minValue = tmp->field;
        while(max!=count){
            tmp=  tmp->next;
            count++;
        }
        int maxValue = tmp->field;
        tmp->field = minValue;
        tmp = list_head;
        count =0;
        while(min!=count){
            tmp=  tmp->next;
            count++;
        }
        tmp->field = maxValue;
    }

    ~LinkedList() {
    while (list_head != NULL) {
        auto tmp = list_head->next;
        delete(list_head);
        list_head = tmp;
        }
    }
};