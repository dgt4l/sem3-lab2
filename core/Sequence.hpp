#pragma once
#include <iostream>

using namespace std;

template<class T>
class Sequence{
    virtual int getLength() = 0;
	
	virtual T getFirst() = 0;

	virtual T getLast() = 0;

	virtual T get(int index) = 0;

	virtual void append(T item) = 0;

	virtual void prepend(T item) = 0;

	virtual void swap(int i1, int i2) = 0;

	virtual void insertAt(T item, int index) = 0;

	virtual void print() = 0;
};