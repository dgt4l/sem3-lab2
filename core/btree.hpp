#pragma once


typedef struct btNode *bTree;

bTree BTreeCreate(void);

void BTreeDestroy(bTree t);

int BTreeSearch(bTree t, int key);

void BTreeInsert(bTree t, int key);

void BTreePrintKeys(bTree t);