#pragma once
#include <iostream>
#include "../Sequence.hpp"
#include "DynamicArray.hpp"

using namespace std;

template <class T> 
class ArraySequence : public Sequence<T> {
private:
	DynamicArray<T>* items;
public:
	ArraySequence(int size) {
		items = new DynamicArray<T>(size);
	}

	ArraySequence(T* arr, int count) {
		items = new DynamicArray<T>(arr, count);
	}

	ArraySequence(ArraySequence<T>& list) {
		this->items = new DynamicArray<T>(list.getLength());
		for (int i = 0; i < list.getLength(); i++)
			this->items->setData(list.get(i), i);
	}

	~ArraySequence() {
		items->~DynamicArray();
	}

	void print() override {
		items->print();
	}

	int getLength() override{
		return items->getSize();
	}

	T getFirst() override {
		return items->getData(0);
	}

	T getLast() override {
		return items->getData(items->getSize() - 1);
	}

	void swap(int ind1, int ind2) override{
        int data_ind1 = ind1;
        int data_ind2 = ind2;
        T data1 = get(data_ind1);
        T data2 = get(data_ind2);
        insertAt(data1, data_ind2);
        insertAt(data2, data_ind1);
    }

	T get(int index) override {
		return items->getData(index);
	}

	void append(T item) override {
		items->resize(items->getSize() + 1);
		items->setData(item, items->getSize() - 1);
	}

	void prepend(T item) override {
		items->resize(items->getSize() + 1);
		for (int i = items->getSize() - 1; i > 0; i--) {
			T tmp = items->getData(i);
			items->setData(items->getData(i - 1), i);
			items->setData(tmp, i-1);
		}
		items->setData(item, 0);
	}

	T &operator[](int index) {
		return get(index);
	}

	int findMax(){
		int max = -10000;
		for(int i = 0; i < items->getSize(); i++){
			if (items->getData(i) > max){
				max = items->getData(i);
			}
		}
		return max;
	}

	void insertAt(T item, int index) override {
		items->setData(item, index);
	}

	pair<int*, int> arrToIntArr() {
		int* res = new int[getLength()];
		for (int i = 0; i < getLength(); i++)
			res[i] = get(i);
		return make_pair(res, getLength());
	}

};